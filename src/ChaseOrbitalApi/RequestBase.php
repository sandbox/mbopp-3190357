<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

use Drupal\commerce_chase\Event\ChaseEvents;
use Drupal\commerce_chase\Event\SoapRequestEvent;

abstract class RequestBase implements RequestInterface {

  /**
   * The soap gateway..
   *
   * @var \Drupal\commerce_chase\ChaseOrbitalApi\SoapGateway;
   */
  protected $gateway;

  /**
   * The soap client.
   *
   * @var \SoapClient
   */
  protected $soapClient;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_chase\ChaseOrbitalApi\SoapGateway $gateway
   *   The Chase gateway.
   */
  public function __construct(SoapGateway $gateway) {
    $this->gateway = $gateway;
    $options = [
      'exceptions' => TRUE,
      'trace' => TRUE,
    ];
    $this->soapClient = new \SoapClient($gateway->getWsdl(), $options);
  }

  /**
   * Send request.
   *
   * @param array $data
   *   An array of input data.
   *
   * @return object
   *   The SOAP response.
   */
  public function send(array $data) {
    $required_keys = $this->getRequiredKeys();
    $this->validateParameters($data, $required_keys);
    $request_data = $this->getParameters($data);
    $request_type = $this->getRequestType();

    $event = new SoapRequestEvent($request_type, $request_data);
    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher */
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch(ChaseEvents::SOAP_REQUEST, $event);

    return $this->soapClient->{$request_type}(new \SoapParam($event->getRequestData(), $request_type));
  }

  /**
   * Validate input data.
   *
   * @param array $data
   *   The input data to validate.
   */
  protected function validateParameters(array $data, array $pm_names) {
    foreach ($pm_names as $pm_name) {
      if (!isset($data[$pm_name])) {
        throw \InvalidArgumentException('Missing parameter ' . $pm_name . 'for charging a profile.');
      }
    }
  }

  /**
   * Adds user and password if required for request.
   *
   * @param object $request
   *   The request to add authentication properties to. (Pass by Ref.)
   *
   * @return object
   *   Original request with authentication properties added if enabled.
   */
  protected function authenticateRequest($request) {
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $gateway_plugin */
    $gateway_plugin = $this->gateway->getGateway()->getPlugin();
    $configuration = $gateway_plugin->getConfiguration();

    if (!$configuration['ip_based_auth'] || $this->requiresCredentialAuthentication()) {
      $request->orbitalConnectionUsername = $configuration['api_username'];
      $request->orbitalConnectionPassword = $configuration['api_password'];
    }

    return $request;
  }

  /**
   * This function can be used to force credentials to the Orbital API.
   *
   * Certain requests require credentials, regardless of whether IP based
   * authentication is being used. (example: ProfileDelete)
   *
   * @see \Drupal\commerce_chase\ChaseOrbitalApi\ProfileDelete
   *
   * @return bool
   *   Whenther or not the request MUST send credentials.
   */
  protected function requiresCredentialAuthentication() {
    return FALSE;
  }

}
