<?php

namespace Drupal\commerce_chase\PluginForm;

use Drupal\commerce_chase\Event\ChaseEvents;
use Drupal\commerce_chase\Event\BuildIframeEvent;
use Drupal\commerce_chase\Event\BuildIframeQueryEvent;
use Drupal\commerce\InlineFormManager;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\commerce_chase\HPSClient;
use Drupal\Component\Utility\Variable;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Checkout form handler for change payment gateway.
 */
class HostedPaymentFormForm extends PaymentMethodAddForm implements ContainerInjectionInterface {

  protected const CHASE_UID_SESSION = 'chase-uid-session-value';

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $token;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The HPS Client.
   *
   * @var \Drupal\commerce_chase\HPSClient
   */
  protected $hpsClient;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Private TempStore / Session service.
   *
   * @var \Drupal\core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Constructs a new HostedPaymentFormForm.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $token
   *   The token generator.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time.
   * @param \Drupal\commerce_chase\HPSClient $hps_client
   *   HPS Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store
   *   Temp Store Factory.
   */
  public function __construct(CurrentStoreInterface $current_store, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager, LoggerInterface $logger, EventDispatcherInterface $event_dispatcher, CsrfTokenGenerator $token, RequestStack $request_stack, TimeInterface $time, HPSClient $hps_client, ConfigFactoryInterface $config_factory, PrivateTempStoreFactory $temp_store) {
    parent::__construct($current_store, $entity_type_manager, $inline_form_manager, $logger);
    $this->eventDispatcher = $event_dispatcher;
    $this->token = $token;
    $this->request = $request_stack->getCurrentRequest();
    $this->time = $time;
    $this->hpsClient = $hps_client;
    $this->configFactory = $config_factory;
    $this->tempStore = $temp_store->get('commerce_chase');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_store.current_store'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('event_dispatcher'),
      $container->get('csrf_token'),
      $container->get('request_stack'),
      $container->get('datetime.time'),
      $container->get('commerce_chase.hps_client'),
      $container->get('config.factory'),
      $container->get('user.private_tempstore')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $form_state->getBuildInfo()['callback_object'];
    $order = $checkout_flow->getOrder();
    $element['#order'] = $order;
    $config = $this->configFactory->get('commerce_chase.settings');
    $remote_id = $order->id() . '-' . $this->time->getRequestTime();
    $library = $this->entity->getPaymentGateway()->getPlugin()->getMode() === 'live' ? 'commerce_chase/hosted-payment-form-live' : 'commerce_chase/hosted-payment-form-test';
    $chase_uid = $this->getFormChaseUid($element, $form_state, $remote_id);

    $user_input = $form_state->getUserInput();
    // $chase_uid_metadata = $this->tempStore->getMetadata(self::CHASE_UID_SESSION);
    // $stored_chase_uid = $this->tempStore->get(self::CHASE_UID_SESSION);
    // $chase_uid_is_old = $chase_uid_metadata->getUpdated() < strtotime('-60 seconds');

    // The chase token that accompanies submitted payment information.
    $element['submitted_chase_uid'] = [
      '#type' => 'hidden',
      '#value' => isset($user_input['payment_information']['add_payment_method']['payment_details']['new_chase_uid']) ? $user_input['payment_information']['add_payment_method']['payment_details']['new_chase_uid'] : NULL, //$chase_uid,
    ];
    // A new chase token to read upon next submission of payement information.
    $element['new_chase_uid'] = [
      '#type' => 'hidden',
      '#value' => $chase_uid,
    ];
    // An ID that Drupal generates to identify a transaction.
    $element['remote_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => ['chase-remote-id'],
      ],
    ];

    $element['status'] = [
      '#prefix' => '<div id="chase-hps-status" class="status-wrapper">',
      '#suffix' => '</div>',
      '#weight' => -11,
    ];

    $element['messages'] = [
      '#prefix' => '<div id="chase-hps-messages" class="message-wrapper error">',
      '#suffix' => '</div>',
      '#markup' => '<ul></ul>',
      '#weight' => -10,
    ];

    $element['iframe'] = [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#weight' => 0,
      '#attributes' => [
        'src' => $this->buildCheckoutIframeUrl($chase_uid),
        'name' => 'embedded-orbital-form',
        'scrolling' => 'no',
        'frameborder' => 0,
        'class' => ['commerce-chase-orbital-hpf-iframe'],
        'id' => 'commerce-chase-orbital-hpf-iframe',
        'width' => '490px',
        'height' => '270px',
      ],
      '#attached' => [
        'library' => [$library],
        'drupalSettings' => [
          'commerce_chase' => [
            'remoteId' => $remote_id,
            'hpsDetailCodes' => $config->get('hpsDetailCodes'),
            'hpsOrbitalCodes' => $config->get('hpsOrbitalCodes'),
          ],
        ],
      ],
    ];

    $event = new BuildIframeEvent($element, $form_state);
    $this->eventDispatcher->dispatch(ChaseEvents::BUILD_IFRAME, $event);
    return $event->getForm();
  }

  /**
   * Builds the hosted form iFrame URL.
   *
   * @param string $chase_uid
   *   The unique HPS ID.
   *
   * @return string
   *   URL for hosted form iFrame.
   */
  protected function buildCheckoutIframeUrl($chase_uid) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_method->getPaymentGateway()->getPlugin();
    $base_url = $payment_gateway_plugin->serviceUrl();

    if (isset($chase_uid)) {
      return Url::fromUri($base_url, [
        'query' => [
          'uID' => $chase_uid,
        ],
      ])->toString();
    }

    return NULL;
  }

  /**
   * Builds the hosted form iFrame URL.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $remote_id
   *   The remote payment method id (aka customer reference number).
   *
   * @return string
   *   URL for hosted form iFrame.
   */
  protected function getFormChaseUid(array $element, FormStateInterface $form_state, string $remote_id) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_gateway_configuration = $payment_method->getPaymentGateway()->getPluginConfiguration();

    $order = $element['#order'];
    // Discern the allowed card types.
    $allowed_types = array_filter($payment_gateway_configuration['allowed_types']);
    $host = $this->request->getSchemeAndHttpHost();;

    $query = [
      'hostedSecureID' => $payment_gateway_configuration['hosted_secure_id'],
      'hostedSecureAPIToken' => $payment_gateway_configuration['hosted_secure_api_token'],
      'action' => 'buildForm',
      'css_url' => $host . '/' . drupal_get_path('module', 'commerce_chase') . '/css/hpf.css',
      'allowed_types' => implode('|', array_values($allowed_types)),
      'collectAddress' => 0,
      'required' => $payment_gateway_configuration['required_info'],
      'cardIndicators' => 'N',
      'hosted_tokenize' => 'store_only',
      'formType' => 5,
      'customer_email' => $order->getEmail(),
      'sessionId' => $this->token->get($order->id()),
      'customerRefNum' => $remote_id,
    ];

    // Allow modules to alter the query parameters for the iframe.
    $event = new BuildIframeQueryEvent($query, $payment_method, $element);
    $this->eventDispatcher->dispatch(ChaseEvents::BUILD_IFRAME_QUERY, $event);

    $chase_uid = $this->hpsClient->requestUniqueId($payment_method->getPaymentGateway(), $event->getQuery());

    $this->tempStore->set(self::CHASE_UID_SESSION, $chase_uid);

    $this->logger->debug('FETCH: ' . $chase_uid);
    return $chase_uid;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    // The payment gateway plugin will process the submitted payment details.
  }

}
