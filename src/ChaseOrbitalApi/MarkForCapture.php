<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

class MarkForCapture extends RequestBase {

  /**
   * {@inheritdoc}
   */
  public function getParameters(array $data) {
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $gateway_plugin */
    $gateway_plugin = $this->gateway->getGateway()->getPlugin();
    $configuration = $gateway_plugin->getConfiguration();
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $data['payment'];

    $mark_for_capture_request = new \stdClass();
    $mark_for_capture_request->bin = $configuration['bin'];
    $mark_for_capture_request->version = '4.0';
    $mark_for_capture_request->merchantID = $configuration['merchant_id'];
    $mark_for_capture_request->amount = $gateway_plugin->toMinorUnits($data['amount']);
    $mark_for_capture_request->orderID = $payment->getOrderId();
    $mark_for_capture_request->txRefNum = $payment->getRemoteId();

    $mark_for_capture_request->txRefIdx = '';
    $mark_for_capture = new \stdClass();
    $mark_for_capture->markForCaptureRequest = $this->authenticateRequest($mark_for_capture_request);
    return $mark_for_capture;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestType() {
    return 'MarkForCapture';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    return ['payment', 'amount'];
  }

}
