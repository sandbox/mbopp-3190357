<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

interface RequestInterface {

  /**
   * Send request.
   *
   * @param array $data
   *   An array of input data.
   * @param array $required_keys
   *   The required keys of the data array.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The SOAP response.
   */
  public function send(array $data);

  /**
   * Request xml parameters for charging a profile.
   *
   * @param array $data
   *   An array of input data.
   *
   * @return array
   *   The request specific xml array in a tag_name => value format.
   */
  public function getParameters(array $data);

  /**
   * Get the required data keys.
   */
  public function getRequiredKeys();

  /**
   * Get the request type.
   *
   * @return string
   *   The CamelCased request type.
   */
  public function getRequestType();



}
