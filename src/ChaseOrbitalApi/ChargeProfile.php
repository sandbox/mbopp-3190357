<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

class ChargeProfile extends RequestBase {

  /**
   * {@inheritdoc}
   */
  public function getParameters(array $data) {
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $gateway_plugin */
    $gateway_plugin = $this->gateway->getGateway()->getPlugin();
    $configuration = $gateway_plugin->getConfiguration();

    $new_order_request = new \stdClass();
    $new_order_request->bin = $configuration['bin'];
    $new_order_request->version = '4.0';
    $new_order_request->merchantID = $configuration['merchant_id'];
    $new_order_request->industryType = 'EC';
    $new_order_request->terminalID = $configuration['terminal_id'];
    $new_order_request->transType = $data['capture'] ? 'AC' : 'A';

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $data['payment_method'];
    if (!$payment_method->get('card_exp_year')->isEmpty() && !$payment_method->get('card_exp_month')->isEmpty()) {
      $new_order_request->ccExp = $payment_method->get('card_exp_year')->value . str_pad($payment_method->get('card_exp_month')->value, 2, 0, STR_PAD_LEFT);
    }
    $new_order_request->useCustomerRefNum = $payment_method->getRemoteId();
    $new_order_request->amount = $gateway_plugin->toMinorUnits($data['price']);
    $new_order_request->orderID = $data['order_id'];
    $new_order_request->mitMsgType = 'CSTO';
    $new_order_request->mitStoredCredentialInd = 'Y';

    $address = $payment_method->getBillingProfile()->get('address');
    $new_order_request->avsZip = $address->postal_code;
    $new_order_request->avsAddress1 = $address->address_line1;
    $new_order_request->avsAddress2 = $address->address_line2;
    $new_order_request->avsCity = $address->locality;
    $new_order_request->avsState = $address->administrative_area;
    $new_order_request->avsCity = $address->given_name . ' ' . $address->family_name;
    $new_order_request->avsCountryCode = in_array($address->country_code, ['US', 'UK', 'CA', 'GB']) ? $address->country_code : '';

    $new_order = new \stdClass();
    $new_order->newOrderRequest = $this->authenticateRequest($new_order_request);

    return $new_order;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    return ['payment_method', 'price', 'capture', 'order_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestType() {
    return 'NewOrder';
  }

}
