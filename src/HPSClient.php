<?php

namespace Drupal\commerce_chase;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use GuzzleHttp\Client;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Utility\Variable;

/**
 * Class HPSClient.
 */
class HPSClient {
  use LoggerChannelTrait;

  /**
   * The guzzle http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Logger Interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new DefaultService object.
   */
  public function __construct(Client $http_client) {
    $this->logger = $this->getLogger('commerce_chase');
    $this->httpClient = $http_client;
  }

  /**
   * Fetches the Chase uID for a Chase HPS Transaction.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   Payment plugin.
   * @param array $query
   *   Querystring array.
   *
   * @return string
   *   Chase uID.
   */
  public function requestUniqueId(PaymentGatewayInterface $payment_gateway, array $query) {

    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    try {
      $request_url = Url::fromUri($payment_gateway_plugin->initUrl(), ['query' => $query])->toString();
      $request_options = [];
      $response = $this->httpClient->get($request_url, $request_options);
      $response_body = (string) $response->getBody();
      parse_str($response_body, $output);
    }
    catch (RequestException $request_exception) {
      $response = $request_exception->getResponse();
      $message = $request_exception->getMessage();
      $message = nl2br(htmlentities($message));
      $this->logger->error('Failed to retrieve HPF iFrame uID: ' . $message);
      return NULL;
    }

    return $output['uID'];
  }

  /**
   * Fetches the Order detail for a Chase HPS Transaction.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   Payment plugin.
   * @param string $chase_uid
   *   The uID to find order for.
   *
   * @return array
   *   Order Details.
   */
  public function requestOrderDetail(PaymentGatewayInterface $payment_gateway, $chase_uid) {
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $payment_gateway_configuration = $payment_gateway->getPluginConfiguration();
    $query = [
      'uID' => $chase_uid,
      'hostedSecureID' => $payment_gateway_configuration['hosted_secure_id'],
      'hostedSecureAPIToken' => $payment_gateway_configuration['hosted_secure_api_token'],
    ];

    $this->logger->debug('Requesting Order Detail for ' . $chase_uid . ': ' . Variable::export($query));
    try {
      $request_url = Url::fromUri($payment_gateway_plugin->queryUrl(), ['query' => $query])->toString();
      $request_options = [];
      $response = $this->httpClient->get($request_url, $request_options);
      $response_body = (string) $response->getBody();

      $this->logger->debug('Order Detail Response for ' . $chase_uid . ': ' . Variable::export($response_body));
      parse_str($response_body, $output);
    }
    catch (RequestException $request_exception) {
      $response = $request_exception->getResponse();
      $message = $request_exception->getMessage();
      $message = nl2br(htmlentities($message));
      $this->logger->error('Failed to retrieve HPF Order Detail for ID ' . $chase_uid . ': ' . $message);
      return NULL;
    }

    return $output;
  }

}
