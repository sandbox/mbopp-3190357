<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

class ProfileFetch extends RequestBase {

  /**
   * {@inheritdoc}
   */
  public function getParameters(array $data) {
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $gateway_plugin */
    $gateway_plugin = $this->gateway->getGateway()->getPlugin();
    $configuration = $gateway_plugin->getConfiguration();

    $profile_fetch_request = new \stdClass();
    $profile_fetch_request->bin = $configuration['bin'];
    $profile_fetch_request->version = '4.0';
    $profile_fetch_request->merchantID = $configuration['merchant_id'];
    $profile_fetch_request->orbitalConnectionUsername = $configuration['api_username'];
    $profile_fetch_request->orbitalConnectionPassword = $configuration['api_password'];
    $profile_fetch_request->customerRefNum = $data['remote_id'];
    $profile_fetch = new \stdClass();
    $profile_fetch->profileFetchRequest = $profile_fetch_request;
    return $profile_fetch;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    return ['remote_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestType() {
    return 'profileFetch';
  }

  /**
   * {@inheritdoc}
   */
  protected function requiresCredentialAuthentication() {
    return TRUE;
  }

}
