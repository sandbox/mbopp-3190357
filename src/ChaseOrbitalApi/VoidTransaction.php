<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi;

class VoidTransaction extends RequestBase {

  /**
   * {@inheritdoc}
   */
  public function getParameters(array $data) {
    /** @var \Drupal\commerce_chase\Plugin\Commerce\PaymentGateway\HostedPaymentForm $gateway_plugin */
    $gateway_plugin = $this->gateway->getGateway()->getPlugin();
    $configuration = $gateway_plugin->getConfiguration();
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $data['payment'];

    $reversal_request = new \stdClass();
    $reversal_request->bin = $configuration['bin'];
    $reversal_request->version = '4.0';
    $reversal_request->merchantID = $configuration['merchant_id'];
    $reversal_request->txRefNum = $payment->getRemoteId();
    $reversal_request->txRefIdx = '';
    $reversal = new \stdClass();
    $reversal->reversalRequest = $this->authenticateRequest($reversal_request);
    return $reversal;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestType() {
    return 'Reversal';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    return ['payment'];
  }

}
