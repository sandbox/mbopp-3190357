<?php

namespace Drupal\commerce_chase\Event;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the Chase iframe query event.
 *
 * @see \Drupal\commerce_chase\Event\PaymentEvents
 */
class BuildIframeQueryEvent extends Event {

  /**
   * The payment method.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentMethodInterface
   */
  protected $paymentMethod;

  /**
   * The url query array.
   *
   * @var array
   */
  protected $query;

  /**
   * The form element.
   *
   * @var array
   */
  protected $element;

  /**
   * Constructs a new iFrame Query Event.
   *
   * @param array $query
   *   The querystring arguments.
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment.
   * @param array $element
   *   The form element being operated on.
   */
  public function __construct(array $query, PaymentMethodInterface $payment_method, array $element) {
    $this->paymentMethod = $payment_method;
    $this->query = $query;
    $this->element = $element;
  }

  /**
   * Gets the payment.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment.
   */
  public function getPaymentMethod() {
    return $this->paymentMethod;
  }

  /**
   * Gets the url query array.
   *
   * @return array
   *   The url query array.
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Sets the query array.
   *
   * @param array $query
   *   The query array.
   */
  public function setQuery(array $query) {
    $this->query = $query;
  }

  /**
   * Gets the form element.
   *
   * @return array
   *   The form element.
   */
  public function getFormElement() {
    return $this->element;
  }

}
