<?php

namespace Drupal\commerce_chase\Event;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the Chase iframe event.
 *
 * @see \Drupal\commerce_chase\Event\PaymentEvents
 */
class BuildIframeEvent extends Event {

  /**
   * The form array.
   *
   * @var array
   */
  protected $form;

  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * Constructs a new PaymentEvent.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   */
  public function __construct(array $form, FormStateInterface $form_state) {
    $this->form = $form;
    $this->formState = $form_state;
  }

  /**
   * Gets the form.
   *
   * @return array
   *   The form.
   */
  public function getForm() {
    return $this->form;
  }

  /**
   *
   * Gets the form_state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The form state..
   */
  public function getFormState() {
    return $this->formState;
  }

  /**
   * Set form array.
   *
   * @param array $form
   *   Form array.
   */
  public function setForm(array $form) {
    $this->form = $form;
  }

  /**
   * Set form state object.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   */
  public function setFormState(FormStateInterface $form_state) {
    $this->formState = $form_state;
  }

}
