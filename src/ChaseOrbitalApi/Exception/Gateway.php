<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi\Exception;

/**
 * Raised when a wrong gateway entity is used for initializing.
  */
class Gateway extends ChaseException {}
