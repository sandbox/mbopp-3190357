<?php

namespace Drupal\commerce_chase\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_chase\ChaseOrbitalApi\SoapGateway;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Exception\SoftDeclineException;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_chase\HPSClient;

/**
 * Provides the Chase Hosted Payment Form off-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "chase_hpf",
 *   label = @Translation("Orbital® Hosted Payment Form"),
 *   display_label = @Translation("Orbital® Hosted Payment Form"),
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_chase\PluginForm\HostedPaymentFormForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = TRUE
 * )
 */
class HostedPaymentForm extends OnsitePaymentGatewayBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The HPS Client.
   *
   * @var \Drupal\commerce_chase\HPSClient
   */
  protected $hpsClient;

  /**
   * Constructs a new BarionPaymentGateway object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\commerce_chase\HPSClient $hps_client
   *   HPS client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerInterface $logger, HPSClient $hps_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->logger = $logger;
    $this->hpsClient = $hps_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('commerce_chase.hps_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getJsLibrary() {
    if ($this->getMode() === 'live') {
      return 'commerce_chase/hosted-payment-form-live';
    }
    return 'commerce_chase/hosted-payment-form-test';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_username' => '',
      'api_password' => '',
      'ip_based_auth' => FALSE,
      'hosted_secure_api_token' => '',
      'hosted_secure_id' => '',
      'merchant_id' => '',
      'terminal_id' => '001',
      'bin' => '000002',
      'allowed_types' => $this->allowedCardTypes(),
      'required_info' => 'minimum',
    ] + parent::defaultConfiguration();
  }

  /**
   * Returns the allowed card types for the API.
   *
   * @return array
   *    Allowed card values.
   */
  public function allowedCardTypes() {
    return array(
      'AmericanExpress' => 'American Express',
      'DinersClub' => 'Diners Club',
      'Discover' => 'Discover',
      'JCB' => 'JCB',
      'MasterCard' => 'MasterCard',
      'Visa' => 'Visa',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['hosted_secure_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secure Account ID'),
      '#description' => $this->t("The merchant's Hosted Payment account"),
      '#required' => TRUE,
      '#default_value' => $this->configuration['hosted_secure_id'],
    ];

    $form['hosted_secure_api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secure Account API Token'),
      '#description' => $this->t("The merchant's Hosted Payment token"),
      '#required' => TRUE,
      '#default_value' => $this->configuration['hosted_secure_api_token'],
    ];

    $form['ip_based_auth'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('IP Based Authentication'),
      '#description' => $this->t('Should the Orbital API authenticate with an API user and password, or will the calls be limited and authenticated by IP address.'),
      '#default_value' => $this->configuration['ip_based_auth'],
    ];

    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Username'),
      '#description' => $this->t('The Orbital connection username'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['api_username'],
    ];

    $form['api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Password'),
      '#description' => $this->t('The Orbital connection password'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['api_password'],
    ];

    $form['terminal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal ID'),
      '#description' => $this->t('Chase Orbital Gateway TerminalID'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['terminal_id'],
    ];

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Your Merchant ID is different from the username which you used to login to your Chase Orbital account. Once you login, browse to your Account tab to find the Merchant ID. If you are using a new Chase Orbital account, you may still need to generate an ID.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['merchant_id'],
    ];

    $form['bin'] = [
      '#type' => 'select',
      '#title' => t('Bin Number'),
      '#options' => [
        '000001' => t('Stratus'),
        '000002' => t('PNS'),
      ],
      '#default_value' => $this->configuration['bin'],
    ];

    $form['allowed_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Payment option icons to show on the checkout form'),
      '#description' => $this->t('Your payment processor and account settings may limit which of these payment options are actually available on the payment form.'),
      '#options' => $this->allowedCardTypes(),

      '#default_value' => $this->configuration['allowed_types'],
    ];

    $form['required_info'] = [
      '#type' => 'radios',
      '#title' => $this->t('Required card information'),
      '#options' => [
        'minimum' => $this->t('Card Number and Expiration Date (mandatory), Name on Card and CVV (optional)'),
        'all' => $this->t('All fields mandatory'),
      ],
      '#default_value' => $this->configuration['required_info'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['hosted_secure_id'] = $values['hosted_secure_id'];
    $this->configuration['hosted_secure_api_token'] = $values['hosted_secure_api_token'];
    $this->configuration['api_username'] = $values['api_username'];
    $this->configuration['api_password'] = $values['api_password'];
    $this->configuration['terminal_id'] = $values['terminal_id'];
    $this->configuration['merchant_id'] = $values['merchant_id'];
    $this->configuration['bin'] = $values['bin'];
    $this->configuration['allowed_types'] = $values['allowed_types'];
    $this->configuration['required_info'] = $values['required_info'];
  }

  /**
   * Returns service URLs for the API.
   *
   * @return string
   *    The service URL.
   */
  public function serviceUrl() {
    switch ($this->getMode()) {
      case 'test':
        return 'https://www.chasepaymentechhostedpay-var.com/hpf/1_1/';

      case 'live':
        return 'https://www.chasepaymentechhostedpay.com/hpf/1_1/';

      default:
        return 'https://www.chasepaymentechhostedpay.com/hpf/1_1/';
    }
  }

  /**
   * Returns init URLs for the API.
   *
   * @return string
   *    The initiation URL.
   */
  public function initUrl() {
    switch ($this->getMode()) {
      case 'test':
        return 'https://www.chasepaymentechhostedpay-var.com/direct/services/request/init';

      case 'live':
        return 'https://www.chasepaymentechhostedpay.com/direct/services/request/init';

      default:
        return 'https://www.chasepaymentechhostedpay.com/direct/services/request/init';
    }
  }

  /**
   * Returns query URLs for the API.
   *
   * @return string
   *   The query URL.
   */
  public function queryUrl() {
    switch ($this->getMode()) {
      case 'test':
        return 'https://www.chasepaymentechhostedpay-var.com/direct/services/request/query';

      case 'live':
        return 'https://www.chasepaymentechhostedpay.com/direct/services/request/query';

      default:
        return 'https://www.chasepaymentechhostedpay.com/direct/services/request/query';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentState($payment_id, $remote_state) {
    $payment_storage = \Drupal::entityTypeManager()
      ->getStorage('commerce_payment');
    $payment = $payment_storage->loadByProperties([
      'remote_id' => $payment_id,
      'payment_gateway' => $this->parentEntity->id(),
    ]);
    if (count($payment) === 1) {
      /** @var \Drupal\commerce_payment\Entity\Payment $payment */
      $payment = reset($payment);
      $payment->setState($this->mapState($remote_state->Status));
      $payment->setRemoteState($remote_state->Status);
      $order = $payment->getOrder();
      if ($order->getData('barion_payment_id') === $payment_id) {
        $payment->save();
      }
      else {
        throw new PaymentGatewayException('Not allowed to update payment in case payment is not active payment on order.');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);
    $payment_gateway = $payment->getPaymentGateway();
    $soap_gateway = new SoapGateway($payment_gateway);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    $order_number = $order ? $order->getOrderNumber() : NULL;
    $data = [
      'order_id' => $order_number ?? $payment->getOrderId(),
      'payment_method' => $payment->getPaymentMethod(),
      'price' => $payment->getAmount(),
      'capture' => $capture,
    ];
    try {
      $response = $soap_gateway->chargeProfile()->send($data);
    }
    catch (\SoapFault $e) {
      $this->logger->info($e->getMessage());
      if (strpos($e->getMessage(), ':')) {
        $message_array = explode(':', $e->getMessage(), 2);
        $code = $message_array[0];
        $message = trim($message_array[1]);
      }
      else {
        $message = $e->getMessage();
        $code = $e->getCode();
      }
      if (in_array($code, [1003, 1004, 1005, 1006, 1007, 1008])) {
        throw new HardDeclineException($message, $code);
      }
      elseif (in_array($code, [1, 2, 3, 6, 14, 24, 26, 29, 30, 40, 47, 49, 50,
        51, 201, 202, 203, 204, 205, 207, 208, 209, 210, 301, 302, 303, 304,
        310, 311, 312, 313, 314, 315, 343, 345, 410, 411, 920, 921, 1002, 1009,
        2002, 3004, 3005, 3016, 4006, 7002, 8001, 9736, 9737, 9738, 9759, 9990,
        9991, 9992, 9993, 9999, 10011, 10138, 10144]) ||
        ($code >= 10000 && $code <= 11000 && !in_array($code, [10005, 10096, 10150, 10204, 10336, 10337, 11101]))) {
        throw new SoftDeclineException($message, $code);
      }
      elseif (in_array($code, [4, 5, 7, 13, 15, 16, 17, 18, 19, 20, 21, 22,
        23, 25, 27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39, 41, 42, 43, 44, 45,
        46, 48, 52, 53, 54, 206, 211, 250, 251, 252, 253, 316, 317, 318, 319,
        320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333,
        334, 335, 336, 337, 338, 339, 340, 341, 342, 344, 346, 347, 348, 350,
        351, 352, 353, 354, 355, 356, 400, 509, 510, 511, 512, 513, 514, 515,
        516, 517, 518, 519, 520, 521, 522, 523, 524, 600, 601, 602, 603, 604,
        605, 606, 607, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932,
        933, 934, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1020,
        2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014,
        3003, 3006, 3008, 3009, 3010, 3011, 3012, 3013, 3014, 3015, 3017, 4002,
        4003, 4005, 4007, 4008, 4010, 4011, 4012, 6002, 6003, 6005, 6006, 6007,
        6008, 6009, 6010, 6011, 6012, 7003, 7004, 8002, 8003, 8004, 8005, 8006,
        8007, 8008, 8009, 8010, 8011, 9739, 9740, 9743, 9744, 9745, 9746, 9747,
        9748, 9749, 9750, 9751, 9752, 9753, 9754, 9755, 9756, 9757, 9758,
        10005, 10096, 10150, 10204, 10336, 10337, 11101]) ||
        ($code >= 700 && $code <= 907) ||
        ($code >= 9001 && $code <= 9735) ||
        ($code >= 9760 && $code <= 9890) ||
        $code >= 19000) {
        throw new InvalidRequestException($message, $code);
      }
      throw new PaymentGatewayException($message, $code);
    }
    catch (\Exception $e) {
      throw new PaymentGatewayException($e->getMessage(), $e->getCode());
    }
    $approval_status = $response->return->approvalStatus;
    if ($approval_status === '0') {
      throw new HardDeclineException(
        $response->return->procStatusMessage,
        intval($response->return->respCode)
      );
    }
    if ($approval_status === '2') {
      throw new InvalidResponseException(
        $response->return->procStatusMessage,
        intval($response->return->respCode)
      );
    }

    $state = $capture ? 'completed' : 'authorization';
    $payment->setState($state);
    $payment->setRemoteId($response->return->txRefNum);
    $payment->save();
    // If the customer did not opt-in to storing the payment method we only
    // remove the remote payment instrument and keep the local one.
    if (!$payment_method->isReusable()) {
      try {
        $soap_gateway = new SoapGateway($payment_method->getPaymentGateway());
        $soap_gateway->profileDelete()->send(['remote_id' => $payment_method->getRemoteId()]);
      }
      catch (\Exception $e) {
        throw new PaymentGatewayException($e->getMessage(), $e->getCode());
      }
      $payment_method->setRemoteId('')->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    try {
      $soap_gateway = new SoapGateway($payment->getPaymentGateway());
      $soap_gateway->voidTransaction()->send(['payment' => $payment]);
    }
    catch (\Exception $e) {
      $this->logger->warning($e->getMessage());
      throw new PaymentGatewayException($this->t('We could not void your payment with Chase. Please try again or contact us if the problem persists.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    $amount = $amount ?: $payment->getAmount();
    try {
      $soap_gateway = new SoapGateway($payment->getPaymentGateway());
      $soap_gateway->captureTransaction()->send(['payment' => $payment, 'amount' => $this->toMinorUnits($amount)]);
    }
    catch (\Exception $e) {
      $this->logger->warning($e->getMessage());
      throw new PaymentGatewayException($this->t('We could not capture your payment with Chase. Please try again or contact us if the problem persists.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    try {
      $soap_gateway = new SoapGateway($payment_method->getPaymentGateway());
      $soap_gateway->profileDelete()->send(['remote_id' => $payment_method->getRemoteId()]);
    }
    catch (\Exception $e) {
      $this->logger->warning($e->getMessage());
    }
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      'remote_id',
      'submitted_chase_uid',
    ];

    $required_response_keys = [
      'customerRefNum',
      'type',
      'exp',
      'mPAN',
      'code',
      'TxnGUID',
    ];

    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    try {
      $order_details = $this->hpsClient->requestOrderDetail($this->parentEntity, $payment_details['submitted_chase_uid']);
    }
    catch (\Exception $e) {
      $this->logger->warning('Could not capture payment details from Chase based on the "uID". @message', [
        '@message' => $e->getMessage(),
      ]);
      throw new PaymentGatewayException($this->t('We could not capture your payment with Chase. Please try again or contact us if the problem persists.'));
    }

    $missing_detail_keys = [];
    foreach ($required_response_keys as $required_key) {
      if (empty($order_details[$required_key])) {
        $missing_detail_keys[] = $required_key;
      }
    }
    if ($missing_detail_keys) {
      $this->logger->warning('$order_details must contain the keys– @keys. Response: @response', [
        '@keys' => implode(', ', $missing_detail_keys),
        '@response' => implode(', ', $order_details),
      ]);

      throw new PaymentGatewayException($this->t('We could not capture your payment with Chase. Please try again or contact us if the problem persists.'));
    }
    else {
      try {
        $card_type = $this->mapCreditCardType($order_details['type']);
      }
      catch (\Exception $e) {
        $this->logger->warning('Unable to map Chase card type. ' . $e->getMessage());
      }
      $payment_method->setRemoteId($order_details['customerRefNum']);
      $payment_method->card_type = $card_type;
      $payment_method->card_exp_month = substr($order_details['exp'], 0, 2);
      $payment_method->card_exp_year = '20' . substr($order_details['exp'], 2);
      $payment_method->card_number = $order_details['mPAN'];
      $payment_method->cardholder_name = $order_details['name'];
    }

    $expires = CreditCard::calculateExpirationTimestamp($payment_method->card_exp_month->value, $payment_method->card_exp_year->value);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * Maps the Chase credit card type to a Commerce credit card type.
   *
   * @param string $card_type
   *   The Chase credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  protected function mapCreditCardType($card_type) {
    $map = [
      'American Express' => 'amex',
      'Diners Club' => 'dinersclub',
      'Discover' => 'discover',
      'JCB' => 'jcb',
      'Mastercard' => 'mastercard',
      'Visa' => 'visa',
    ];
    if (!isset($map[$card_type])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $card_type));
    }

    return $map[$card_type];
  }

}
