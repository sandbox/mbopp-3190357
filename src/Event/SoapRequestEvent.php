<?php

namespace Drupal\commerce_chase\Event;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the Chase SOAP Request event.
 *
 * @see \Drupal\commerce_chase\Event\PaymentEvents
 */
class SoapRequestEvent extends Event {

  /**
   * The request type.
   *
   * @var string
   */
  protected $requestType;

  /**
   * The request parameter hash.
   *
   * @var object
   */
  protected $requestData;

  /**
   * Constructs a new PaymentEvent.
   *
   * @param string $request_type
   *   The request type.
   * @param object $request_data
   *   The request parameter data.
   */
  public function __construct($request_type, $request_data) {
    $this->requestType = $request_type;
    $this->requestData = $request_data;
  }

  /**
   * Gets the request type.
   *
   * @return string
   *   The request type string.
   */
  public function getRequestType() {
    return $this->requestType;
  }

  /**
   * Gets the request parameter data.
   *
   * @return object
   *   The request parameters.
   */
  public function getRequestData() {
    return $this->requestData;
  }

  /**
   * Sets the request data.
   *
   * @param object $request_data
   *   The request parameter data.
   */
  public function setRequestData($request_data) {
    $this->requestData = $request_data;
  }

}
