(function($, Drupal, window) {

  var authorizationMessageTimeout = null;

  Drupal.behaviors.chaseOrbitalHpf = {
    attach: function (context, settings) {
      window.cancelCREPayment = this.cancelCREPayment;
      window.whatCVV2 = this.whatCVV2;
      window.creHandleErrors = this.creHandleErrors;
      window.creHandleDetailErrors = this.creHandleDetailErrors;
      window.startCREPayment = this.startCREPayment;
      window.completeCREPayment = this.completeCREPayment;
      window.startPayment = this.startPayment;

      // Hide the next checkout step button since the iframe has its own.
      if ($('#commerce-chase-orbital-hpf-iframe').length) {
        $('[data-drupal-selector="edit-actions-next"]').hide();
      }
      else {
        $('[data-drupal-selector="edit-actions-next"]').show();
      }
    },
    whatCVV2: function () {
      var string = Drupal.t('The CVV Number ("Card Verification Value") on your credit card or debit card is a 3 digit number on VISA®, MasterCard® and Discover® branded credit and debit cards. On your American Express® branded credit or debit card it is a 4 digit numeric code.');
      // @todo add Drupal.theme implementation and preprend above iframe, or something.
      alert(string);
    },
    creHandleErrors: function (errorCode) {
      console.log('Error', errorCode);
    },
    creHandleDetailErrors: function (errorCode, gatewayCode, gatewayMessage) {
      var errorCodes = errorCode.split('|');
      var $messageWrapper = $('#commerce-chase-orbital-hpf-iframe').siblings('.message-wrapper').find('ul').empty();
      clearTimeout(authorizationMessageTimeout);
      $('#commerce-chase-orbital-hpf-iframe').siblings('.status-wrapper').empty();
      $.each(errorCodes, function(index, code) {
        if (code) {
          var msg = code;
          if (drupalSettings.commerce_chase.hpsDetailCodes[code]) {
            msg = drupalSettings.commerce_chase.hpsDetailCodes[code];
          }
          if (gatewayMessage) {
            msg = gatewayMessage;
          }
          $('<li>' + msg + '</li>').appendTo($messageWrapper);
        }
      });
    },
    startPayment: function () {
      $('#commerce-chase-orbital-hpf-iframe').siblings('.message-wrapper').find('ul').empty();
      authorizationMessageTimeout = setTimeout(function() {
        $('#commerce-chase-orbital-hpf-iframe').siblings('.status-wrapper').html('<div class="authorizing"><span>Authorizing Payment</span></div>');
      }, 500);
    },
    startCREPayment: function () {
      //
    },
    completeCREPayment: function (transaction) {
      $('#commerce-chase-orbital-hpf-iframe').siblings('.status-wrapper').html('<div class="complete"><span>Authorization Complete</span></div>');
      $('.chase-remote-id').val(this.drupalSettings.commerce_chase.remoteId);
      $('form.commerce-checkout-flow').submit();
    }
  }

})(jQuery, Drupal, window);
