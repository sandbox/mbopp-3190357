<?php

namespace Drupal\commerce_chase\Event;

final class ChaseEvents {

  /**
   * Provides possibility to alter the iframe url query.
   *
   * @Event
   *
   * @see \Drupal\commerce_chase\Event\BuildIframeQueryEvent
   */
  const BUILD_IFRAME_QUERY = 'commerce_chase.build_iframe_query';


  /**
   * Provides possibility to alter the iframe attributes.
   *
   * @Event
   *
   * @see \Drupal\commerce_chase\Event\BuildIframeEvent
   */
  const BUILD_IFRAME = 'commerce_chase.build_iframe';

  /**
   * Provides possibility to alter and debug the request.
   *
   * @Event
   *
   * @see \Drupal\commerce_chase\Event\SoapRequestEvent
   */
  const SOAP_REQUEST = 'commerce_chase.soap_request';

}
