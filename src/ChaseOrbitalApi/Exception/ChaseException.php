<?php

namespace Drupal\commerce_chase\ChaseOrbitalApi\Exception;

/**
 * Parent class for all Chase Orbital exceptions.
 */
class ChaseException extends \Exception {}
